import 'dart:math';
import 'package:flutter/material.dart';

///Painter made to draw an analog clock
class ClockPainter extends CustomPainter {
  
  DateTime datetime;

  // Design variables for the clock
  final bool showTicks;
  final bool showNumbers;
  final bool showSecondHand;
  final Color hourHandColor;
  final Color minuteHandColor;
  final Color secondHandColor;
  final Color tickColor;
  final Color numberColor;
  final double textScaleFactor;

  // Paint variables
  TextPainter indicatorPainter;
  Paint midPointStrokePainter;
  Paint tickPaint;
  Paint handPaint;

  // Constants to paint
  static const double BASE_SIZE = 320.0;
  static const double MINUTES_IN_HOUR = 60.0;
  static const double SECONDS_IN_MINUTE = 60.0;
  static const double MILISECONDS_IN_SECOND = 1000.0;
  static const double HOURS_IN_CLOCK = 12.0;
  static const double HAND_PIN_HOLE_SIZE = 3.0;
  static const double STROKE_WIDTH = 2.0;

  ClockPainter(
    {@required this.datetime,
      this.showTicks = true,
      this.showNumbers = true,
      this.showSecondHand = true,
      this.hourHandColor = Colors.black,
      this.minuteHandColor = Colors.black,
      this.secondHandColor = Colors.redAccent,
      this.tickColor = Colors.grey,
      this.numberColor = Colors.black,
      this.textScaleFactor = 1.0}
  ) {
    indicatorPainter = new TextPainter(
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr
    );
    midPointStrokePainter = Paint()
      ..color = showSecondHand ? secondHandColor : minuteHandColor
      ..isAntiAlias = true
      ..style = PaintingStyle.stroke;
    tickPaint = Paint()
      ..color = tickColor;
    handPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..strokeJoin = StrokeJoin.bevel;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double scaleFactor = size.shortestSide / BASE_SIZE;

    if (showTicks) _paintTickMarks(canvas, size, scaleFactor);
    if (showNumbers) _drawIndicators(canvas, size, scaleFactor);

    _paintClockHands(canvas, size, scaleFactor);
    _paintPinHole(canvas, size, scaleFactor);
  }

  @override
  bool shouldRepaint(ClockPainter oldDelegate) {
    return oldDelegate?.datetime?.isBefore(datetime) ?? true;
  }

  //Draws the center of the clock where the hands meet
  _paintPinHole(canvas, size, scaleFactor) {
    midPointStrokePainter..strokeWidth = STROKE_WIDTH * scaleFactor;

    canvas.drawCircle(
        size.center(Offset.zero),
        HAND_PIN_HOLE_SIZE * scaleFactor,
        midPointStrokePainter
    );
  }

  /// Draws on the given canvas indicator for hours {3,6,9,12}
  void _drawIndicators(Canvas canvas, Size size, double scaleFactor) {

    // Genero el estilo de los indicadores
    TextStyle indicatorStyle = TextStyle(
        fontFamily: 'Abel',
        color: numberColor,
        fontWeight: FontWeight.bold,
        fontSize: 12.0 * scaleFactor * textScaleFactor
    );

    // Calculate the offsets for the indicators
    double p = 4.0;
    if (showTicks) p += 24.0;
    Offset paddingX = Offset(p * scaleFactor, 0.0);
    Offset paddingY = Offset(0.0, p * scaleFactor);

    // Paint the hour indicators
    TextSpan span3 = new TextSpan(style: indicatorStyle, text: "3");
    TextPainter tp3 = indicatorPainter..text = span3;
    tp3.layout();
    tp3.paint(canvas, size.centerRight(-tp3.size.centerRight(paddingX)));

    TextSpan span6 = new TextSpan(style: indicatorStyle, text: "6");
    TextPainter tp6 = indicatorPainter..text = span6;
    tp6.layout();
    tp6.paint(canvas, size.bottomCenter(-tp6.size.bottomCenter(paddingY)));

    TextSpan span9 = new TextSpan(style: indicatorStyle, text: "9");
    TextPainter tp9 = indicatorPainter..text = span9;
    tp9.layout();
    tp9.paint(canvas, size.centerLeft(-tp9.size.centerLeft(-paddingX)));

    TextSpan span12 = new TextSpan(style: indicatorStyle, text: "12");
    TextPainter tp12 = indicatorPainter..text = span12;
    tp12.layout();
    tp12.paint(canvas, size.topCenter(-tp12.size.topCenter(-paddingY)));
  }

  Offset _getHandOffset(double percentage, double length) {
    final radians = 2 * pi * percentage;
    final angle = -pi / 2.0 + radians;

    return new Offset(length * cos(angle), length * sin(angle));
  }

  void _paintTickMarks(Canvas canvas, Size size, double scaleFactor) {

    // Set sizes for all the elements
    double r = size.shortestSide / 2;
    double tick = 5 * scaleFactor, mediumTick = 1.5 * tick, longTick = 2.0 * tick;
    double p = longTick + 4 * scaleFactor;

    // Set the width for the ticks
    tickPaint..strokeWidth = 1.0 * scaleFactor;

    // 60 ticks in the clock face
    for (int i = 1; i <= 60; i++) {

      // Regular size ticks (1x)
      double len = tick;

      // Every 15 ticks (2x longer)
      if (i % 15 == 0) { len = longTick;}

      // Every 5 ticks (1.5x longer)
      else if (i % 5 == 0) { len = mediumTick; }

      // Get the angle from 12:00 to this tick in radians
      double angleFrom12 = i / 60.0 * 2.0 * pi;

      // Get the angle from 3:00 to this tick
      // Note: 3 O'Clock corresponds with zero angle in unit circle
      // Makes it easier to do the math.
      double angleFrom3 = pi / 2.0 - angleFrom12;

      //draw the tick
      canvas.drawLine(
          size.center(
              Offset(
                  cos(angleFrom3) * (r + len - p),
                  sin(angleFrom3) * (r + len - p)
              )
          ),
          size.center(
              Offset(
                  cos(angleFrom3) * (r - p),
                  sin(angleFrom3) * (r - p)
              )
          ),
          tickPaint
      );
    }
  }

  void _paintClockHands(Canvas canvas, Size size, double scaleFactor) {
    double r = size.shortestSide / 2;

    //offsets for hands
    double p = 0.0;
    if (showTicks) p += 28.0;
    if (showNumbers) p += 24.0;

    //sizes in relation to offsets
    double secondsHandLength = r - (p * scaleFactor);
    double minutesHandLength = r - (p +8)* scaleFactor;
    double hoursHandLength = r - (p + 36.0) * scaleFactor;

    handPaint..strokeWidth = STROKE_WIDTH * scaleFactor;

    // Get all data from the date time variable
    double seconds = (datetime.second+(datetime.millisecond/ MILISECONDS_IN_SECOND))/SECONDS_IN_MINUTE;//datetime.second / SECONDS_IN_MINUTE;
    double minutes = (datetime.minute + seconds) / MINUTES_IN_HOUR;
    double hour = (datetime.hour + minutes) / HOURS_IN_CLOCK;

    //Hour hand
    canvas.drawLine(
        size.center(_getHandOffset(hour, HAND_PIN_HOLE_SIZE * scaleFactor)),
        size.center(_getHandOffset(hour, hoursHandLength)),
        handPaint..color = hourHandColor
    );

    //Minute hand
    canvas.drawLine(
        size.center(_getHandOffset(minutes, HAND_PIN_HOLE_SIZE * scaleFactor)),
        size.center(_getHandOffset(minutes, minutesHandLength)),
        handPaint
          ..color = minuteHandColor
          ..strokeWidth = 1.5 * scaleFactor
    );

    //Second hand
    canvas.drawLine(
        size.center(_getHandOffset(seconds, HAND_PIN_HOLE_SIZE * scaleFactor)),
        size.center(_getHandOffset(seconds, secondsHandLength)),
        handPaint
          ..color = secondHandColor
          ..strokeWidth = scaleFactor
    );
  }
}