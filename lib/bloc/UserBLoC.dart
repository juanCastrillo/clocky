import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

FirebaseUser firebaseUser;

class UserBLoC {

  //TODO - Implement method of keysigning for later cloud funtions auth.

  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth auth = FirebaseAuth.instance;

  ///Signs a user with google and sends it to firebase if it hasn't been done already
  ///@returns if the user is signIn
  Future<bool> signInWithGoogle() async {

    //Sign into google
    if (await googleSignIn.isSignedIn()) {

      //Not registered in firebase auth
      if(auth.currentUser == null){
        final googleAuth = await googleSignIn.currentUser.authentication;
        firebaseUser = (await auth.signInWithCredential(GoogleAuthProvider.getCredential(idToken: googleAuth.idToken, accessToken: googleAuth.accessToken))).user;
        print("Signed In: " + firebaseUser.displayName + ", " +(await firebaseUser.getIdToken()).toString());
        return true;
      }
      else
        return true;
    }

    //Not signed into google
    else {
      await googleSignIn.signIn().then((user) async {
        final googleAuth = await user.authentication;

        firebaseUser = (await auth.signInWithCredential(GoogleAuthProvider.getCredential(idToken: googleAuth.idToken, accessToken: googleAuth.accessToken))).user;
        print("Signed In: " + firebaseUser.displayName);
        return true;

      })
      .catchError((error){
          print(error);
          return false;
      });
      return false;
    }
  }

  Future<bool> signInWithEmail() async {
    return false;
  }

  //TODO add every signIn option check
  Future<bool> checkSignIn() async {
    //Google sign in check
    firebaseUser = await auth.currentUser() ;
    if(await firebaseUser != null){
      return true;
    } else return false;
  }

}