import 'dart:async';
import 'package:clocky/models/Alarm.dart';
import 'package:clocky/models/Events.dart';
import 'package:clocky/models/Time.dart';
import 'package:clocky/resources/AlarmsProvider.dart';

import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/services.dart';

import 'UserBLoC.dart';

///Backend to manage the alarm flow
class AlarmsBLoC {

  /// Repository to get the alarm data
  final _alarmRepository = AlarmsProvider();

  //TODO - add check for alarm sounded event method
  static const platform = const MethodChannel("clocky/alarms");

  /// Alarms cache
  List<Alarm> _alarms = [];

  ///Event Controller
  final _alarmsEventController = StreamController<AlarmEvent>();
  Sink<AlarmEvent> get alarm_event_sink => _alarmsEventController.sink;
  StreamSink<List<Alarm>> get alarms_sink => _alarmsStreamController.sink;
  Stream<List<Alarm>> get stream_alarms => _alarmsStreamController.stream;

  AlarmsBLoC({Alarm alarm}) {
    // When opening the app get all the alarms from cloud repository.
    _alarmRepository.getCloudAlarms()
      .then((alarms) {
        print("Alarmitas: "+alarms.toString());
        _alarms.addAll(alarms);
        alarms_sink.add(alarms);
      })
      .catchError((error){ print("AlarmBLocError: "+error); });
    print("created alarmBloc");

    //Listen for any adition event of alarms
    //_alarmsEventController.stream.listen(_add);
  }

  List<Alarm> addAlarm(Alarm alarm){
    // _alarms.add(AlarmBLoC(alarm: alarm));//await updateAlarm(alarm));
    _alarms.add(alarm);
    alarms_sink.add(_alarms);

    //TODO - call repository save

    return _alarms;
  }

  void removeAlarm(Alarm alarm) async {
    // TODO - repository delete

    if(_alarms.contains(alarm)) {
      _alarms.remove(alarm);
      alarms_sink.add(_alarms);
    }
  }

  /// Platform call
  ///Adds or remove the alarm of the alarmManager and 
  ///returns the alarm updated with the appropriated ids of the alarms set
  Future<Alarm> setAlarm(Alarm alarm) async{

    print("Alarm to set: " + alarm.toString());

    alarm.alarmIds = await _startPlatformAlarm(alarm);

    print("alarmUpdated: " + alarm.alarmIds.toString());

    return alarm;
  }

  //TODO encontrar como se supone que se activan alarmas en Android. En IOs gg fast
  Future<List<int>> _startPlatformAlarm(Alarm alarm) async{
    try {
      List<int> alarmIds = (
        //TODO - Add alarm description to the list
        await platform.invokeMethod('activateAlarms', {"days":alarm.days, "time":[alarm.time.hour, alarm.time.minute]})).cast<int>();
      return alarmIds;
    } on PlatformException catch(e){
      print("startPlatformAlarm error: " + e.message);
    }
  }

  ///Stream Controller
  final _alarmsStreamController = StreamController<List<Alarm>>.broadcast(
    onListen: (){
      print("listening controller");
    },
  );
  //var alarmBroadcast;

  ///Method to add the alarm
  createTempAlarm() async => alarms_sink.add(addAlarm(Alarm(
      time: Time(hour: 0, minute: 0),
      description: "",
      days: [false, false, false, false, false, false, false],
      editing: true,
  )));

  void update(){
    alarms_sink.add(_alarms);
  }

  void dispose() {
    _alarmsStreamController.close();
    _alarmsEventController.close();
  }

  void updateAlarm(Alarm alarm) {
    // Instead of calling list.remove(object) this implementation is done becouse usually the alarm will be the last one
    // Search for the alarm and update it
    print("Alarm to update: "+alarm.toString());
    for(int i = _alarms.length-1; i >= 0; i--) {
      if (_alarms[i] == alarm) {
        print("${_alarms[i].id} == ${alarm.id}");
        print("Alarm[$i]: ${_alarms[i]}");
        _alarms[i] = alarm;
        alarms_sink.add(_alarms);
        break;
      }
    }
  }
}
