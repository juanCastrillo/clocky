import 'package:clocky/widgets/AlarmCardWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'models/Alarm.dart';
import 'models/Time.dart';

class TestPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return TestPageState();
  }

}

class TestPageState extends State<TestPage> {

  double start = 0.0;
  double distance = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Draggable(
              feedback: Container(),
              child: Card(
                elevation: 4.0,
                child: Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Text(""),
                ),
              ),
            childWhenDragging: Card(
              color: Colors.blue,
              elevation: 4.0,
              child: Padding(
                padding: const EdgeInsets.all(40.0),
                child: Text(""),
              ),
            ),
          ),
          Stack(

          )
        ],
      ),
    );
  }

}