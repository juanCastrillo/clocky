import 'package:clocky/bloc/AlarmsBLoC.dart';
import 'package:flutter/material.dart';

import 'models/Alarm.dart';
import 'models/Things.dart';
import 'models/Time.dart';

class AlarmTestPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clocky',
      darkTheme: darkTheme,
      theme: lightTheme,
      home: TPage(),
    );
  }
}

class TPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TPageState();
  }
}

class TPageState extends State<TPage> {

  AlarmsBLoC abloc = AlarmsBLoC();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RaisedButton(
        onPressed: () {
          var now = TimeOfDay.now();
          abloc.setAlarm(
            Alarm(
              time: Time(hour: now.hour, minute: now.minute+1),
              days: [false, false, false, false, false, false, true],
              description: "si funciona funciona",
            )
          );
        }
      ),
    );
  }
}