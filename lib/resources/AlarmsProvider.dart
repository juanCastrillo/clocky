import 'dart:async';
import 'package:cloud_functions/cloud_functions.dart';

import 'package:clocky/models/Alarm.dart';
import 'package:clocky/bloc/UserBLoC.dart';
import 'package:flutter/services.dart';

//TODO implement local saving solution and use cloud as backup and sharedAlarms
/// Provides alarms data from remote sources.
class AlarmsProvider {

  /// Cache of alarms, necesary?
  // List<Alarm> _alarms = [];

  AlarmProvider() {
    // ???
  }

  /// Get all alarms order by
  /// 
  Future<List<Alarm>> getAlarms() async {
    //TODO - implementar forma de coger alarmas (preferiblemente de local DB y comprobar con Remote db para ver si es necesario actualizar)
    return getCloudAlarms();
  }

  /// Gets alarms from firestore
  Future<List<Alarm>> getCloudAlarms() async {

    var getAlarms = CloudFunctions.instance.getHttpsCallable(
      functionName: "getAlarms",
    );

    var response = await getAlarms.call(<String, String>{
      "uid":firebaseUser.uid,
      "key":""
    })
    .catchError((error){
      print("getCloudAlarms"+error.toString());
      return error;
    });

    var rawAlarmData = response.data;
    List<Alarm> alarms = [];
    
    for (var value in rawAlarmData) {
      alarms.add(Alarm.fromJson(value));
    }
    
    return alarms;
  }

  //TODO - Refactor this method, maybe change name and fix functionality adding it to the doc.
  /// Adds an alarm to the alarm list and returns the whole list.
  /// alarm: Alarm 
  Future<List<Alarm>> _saveAlarm(Alarm alarm) async{

    //Save on Firestore
    CloudFunctions.instance.getHttpsCallable(
        functionName: 'addAlarm'
    )
      .call(<String, dynamic>{
        "user": {
          "uid": firebaseUser.uid,
          "key": ""
        },
        //TODO - send reamining data and use method in class for json conversion
        "alarm":{
          "time":alarm.time.toString(),
          "description": alarm.description,
          "days": alarm.days
        }
      })
      .then((response) {
        var resp = response.data;
        print(resp);
      })
      .catchError((error){
        print(error.message);
      });

    // Save on platform channel
    updateAlarm(alarm);
    
  }

  /// Saves an already existing alarm updating it.
  /// alarm: existing Alarm with new data
  void updateAlarm(Alarm alarm) async {
    // TODO - update alarm on firestore

  }
}