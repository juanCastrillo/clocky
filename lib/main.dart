import 'package:clocky/AlarmTest.dart';
import 'package:clocky/AlarmsPage.dart';
import 'package:clocky/ClocksPage.dart';
import 'package:clocky/UserCreationPage.dart';
import 'package:clocky/models/Things.dart';
import 'package:flutter/material.dart';

void main() {
  //TODO - Check if an alarm has come to launch another page
  runApp(Clocky()); 
   
}

class Clocky extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clocky',
      darkTheme: darkTheme,
      theme: lightTheme,
      home: UserCreationPage(),
    );
  }
}

class MainApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainAppState();
  }
}

class MainAppState extends State<MainApp> {

  int selectedIndex = 0;
  final List<Widget> pages = [
    AlarmsPage(),
    ClocksPage() //TODO Evitar que corra aunque no este elegida en la barra
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //TODO fix reloading data (or crashing) when changing between widgets
      body: pages[selectedIndex],
      // IndexedStack(
      //   index: selectedIndex,
      //   children: pages,
      // ),
      bottomNavigationBar: Theme(
        data: Theme.of(context),
        child: BottomNavigationBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.alarm),
              title: Text("Alarms"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.access_time),
              title: Text("Clock"),
            ),
          ],
          currentIndex: selectedIndex,
          onTap: ((int index) {
            setState(() {selectedIndex = index;});
          }),
        ),
      ),
    );
  }
}
