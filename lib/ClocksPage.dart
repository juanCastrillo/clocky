import 'package:clocky/widgets/AnalogClock.dart';
import 'package:clocky/widgets/DigitalClock.dart';
import 'package:flutter/material.dart';

class ClocksPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ClocksPageState();
  }
}

class ClocksPageState extends State<ClocksPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.amber,
      body: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.only(left:48.0, right:48.0, top: 48.0, bottom: 16),
                child: Container(
                  child: AnalogClock(border: false),
                  // decoration: BoxDecoration(
                  //   shape: BoxShape.circle,
                  //   boxShadow: <BoxShadow>[
                  //     BoxShadow(
                  //       spreadRadius: 0.05,
                  //       blurRadius: 8.0,
                  //       color: Colors.grey,
                  //     ),
                  //   ],
                  // ),
                ),
              ),
            ),
            // Center(child: DigitalClock()),
          ],
        ),
      ),
    );
  }
}
