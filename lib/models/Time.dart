import 'Exceptions.dart';

var hours = List.generate(24, (int i)=>i++);
var minutes = List.generate(60, (int j)=>j++);//(int j)=>j*=5);

class Time {
  final MAX_HOUR = 23;
  final MIN = 0;

  final MAX_MINUTE = 59;

  Time({int hour = 0, int minute = 0}){
    if(hour > MAX_HOUR || hour< MIN) throw(HourOutOfRangeException());
    if(minute > MAX_MINUTE || minute < MIN) throw(MinuteOutOfRangeException());
    this.hour = hour; this.minute = minute;
  }

  //TODO, optimize for when there are no 0s like 1:2 instead of 01:02 or 1:02
  ///Adapts to time a hh:mm string format
  Time.fromString(String stringTime){
    if(stringTime.length == 5){
      try {
        hour = int.parse(stringTime[0] + stringTime[1]);
        minute = int.parse(stringTime[3] + stringTime[4]);
      }catch(e){throw(WrongTimeStringFormatException());}
    }
    else {throw(WrongTimeStringFormatException());}//hour= 0; minute=0;}
  }

  int hour;
  int minute;

  String toString(){
    String timeString = "";
    if(hour<10) timeString += "0"+hour.toString();
    else timeString += hour.toString();

    timeString +=":";

    if(minute<10) timeString+="0"+minute.toString();
    else timeString += minute.toString();

    return timeString;
  }

  Duration toDuration(){
    DateTime now = DateTime.now();
    DateTime alarm = DateTime(now.year, now.month, now.day, this.hour, this.minute);
    var delay = alarm.millisecondsSinceEpoch - now.millisecondsSinceEpoch;

    return Duration(milliseconds: delay);
  }
}