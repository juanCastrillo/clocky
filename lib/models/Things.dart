import 'package:flutter/material.dart';

var clock = true;
var defaultColor = Colors.white;

var lightTheme = ThemeData(
  fontFamily: 'Dosis',
  scaffoldBackgroundColor: Colors.white,
  backgroundColor: Colors.white,
  primarySwatch: Colors.amber,
  accentColor: Colors.amberAccent,
  splashColor: Colors.black,
);

var darkTheme = ThemeData(
  fontFamily: 'Dosis',
  scaffoldBackgroundColor: Colors.black,
  backgroundColor: Colors.black,
  primarySwatch: Colors.amber,
  accentColor: Colors.amberAccent,
  cardColor: Colors.grey,
  splashColor: Colors.white,
  textTheme: TextTheme(
    bodyText2: TextStyle(
      color: Colors.white,
    ),
  ),
  bottomAppBarColor: Colors.black

);
