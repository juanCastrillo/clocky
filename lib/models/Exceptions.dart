//final HourOutOfRangeException = Exception("Hour out of range 0-23");
//final MinuteOutOfRangeException = Exception("Minute out of range 0-59");

class MinuteOutOfRangeException implements Exception{
  MinuteOutOfRangeException():super();
  final String exceptionMessage = "Minute out of range 0-59";
}

class HourOutOfRangeException implements Exception{
  HourOutOfRangeException():super();
  final String exceptionMessage = "Hour out of range 0-23";
}

class WrongTimeStringFormatException implements Exception {
  WrongTimeStringFormatException():super();
  final String exceptionMessage = "Wrong time string format, should be hh:mm";
}