import 'Time.dart';
import 'User.dart';
import 'package:random_string/random_string.dart';

//TODO - Hacer interfaz de alarma e implementar la clase alarma unificando la actual y compartida.
class Alarm {

  String id;

  //hour for the alarm to be triggered
  Time time;
  //description about what is the alarm for
  String description;
  //boolean list of days it should ring
  List<bool> days;

  //List of ids for the days of alarms (to be turned on), if 0 no alarm active that day
  List<int> alarmIds=[0,0,0,0,0,0,0];

  bool active;

  bool editing;

  Alarm({this.id:null, this.time, this.description, List<bool> days, this.editing:false, this.active:true}){
    if(id == null) {
      id = randomAlphaNumeric(10);
    }
    if(days.length>7) {
      List<bool> tempDays;
      for (int i = 0; i < 7; i++) {
        tempDays.add(days[i]);
      }
      this.days = tempDays;
    } else this.days = days;
  }

  Alarm.fromJson(Map json){
    this.id = json['id'];
    this.time = Time.fromString(json['time']);
    this.description = json['description'];
    this.days = [];
    try {
      for (int i = 0; i < 7; i++) {
        days.add(json['days'][i] as bool);
      }
    } catch(exception){print(exception);}

    //TODO - Take this variables from the JSON.
    this.active = true;
    this.editing = false;
  }

  Map toJson() {
    //TODO - implement json conversion
    return Map();
  }

  String toString(){
    return "time: $time\n description: $description \n days${days.toString()} \n editing: $editing \n";
  }

  bool operator ==(other){
    return this.id == other.id;
  }

  @override
  int get hashCode => super.hashCode;

}

class SharedAlarm extends Alarm {

  SharedAlarm({Time time, String description, List<bool> days, bool editing = false, bool active})
      :super(time:time, description:description, days:days, editing:editing, active:active);

  SharedAlarm.fromJson(Map json){
    this.id = json['id'];
    this.time = Time.fromString(json['time']);
    this.description = json['description'];
    this.days = [];
    try {
      for (int i = 0; i < 7; i++) {
        days.add(json['days'][i] as bool);
      }
    } catch(exception){print(exception);}
    try {
      for (int i = 0; i < 7; i++) {
        people.add(json['people'][i] as User);
      }
    } catch(exception){print(exception);}
  }

  String id;

  List<User> people;
}