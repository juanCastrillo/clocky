import 'package:clocky/bloc/UserBLoC.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'main.dart';

class UserCreationPage extends StatefulWidget {

  final UserBLoC userBLoC = UserBLoC();

  @override
  State<StatefulWidget> createState() {
    return UserCreationPageState();
  }

}

class UserCreationPageState extends State<UserCreationPage> {

  void advance(){
    Navigator.pushReplacement(context,
      MaterialPageRoute(
        builder: (context) {
          return MainApp();
        },
      ),
    );
  }

  void signInInitiation() async{
    if(await widget.userBLoC.checkSignIn())
      advance();
  }

  @override
  void initState() {
    signInInitiation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GoogleSignInButton(
            onPressed: (){
              widget.userBLoC.signInWithGoogle().then((bool signed) {
                if(signed) {
                  advance();
                }
                else {print("couldn't be signed in");}
              });
            },
            borderRadius: 24.0,
          ),
//          RaisedButton(
//            child: Text(
//              "Other"
//            ),
//          ),
          FlatButton(
            child: Text(
                "Entrar offline"
            ),
            onPressed: (){advance();},
          ),
          //Makes the column fill the horizontal parent
          Container(
            height: 0,
            width: double.infinity,
          )
        ],
      ),
    );
  }

}