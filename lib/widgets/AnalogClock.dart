import 'dart:async';

import 'package:clocky/AnalogClockPainter.dart';
import 'package:flutter/material.dart';

///Analog Clock widget with smooth senconds hand
class AnalogClock extends StatefulWidget {
  bool border = false;

  AnalogClock({this.border});

  @override
  State<StatefulWidget> createState() {
    return AnalogClockState(DateTime.now());
  }
}

class AnalogClockState extends State<AnalogClock> {
  DateTime datetime;

  AnalogClockState(datetime) : this.datetime = datetime ?? DateTime.now();

  initState() {
    super.initState();

    // update clock every 60ms to represent a smooth seconds hand
    Duration updateDuration = Duration(milliseconds: 60);
    Timer.periodic(updateDuration, update);
  }

  update(Timer timer) {
    if (mounted) {
      // update is only called on live clocks. So, it's safe to update datetime.
      datetime = DateTime.now();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: widget.border ? EdgeInsets.all(8.0) : EdgeInsets.all(0.0),
        child: AspectRatio(
          aspectRatio: 1.0,
          child: Container(
            decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),            
            child: CustomPaint(
              painter: ClockPainter(
                datetime: datetime,
                showTicks: true,
                showNumbers: false,
                showSecondHand: true,
                hourHandColor: Colors.black,
                minuteHandColor: Colors.black,
                secondHandColor: Colors.red,
                tickColor: Colors.black,
                numberColor: Colors.black
              ),
            ),
          ),
        ),
      ),
    );
  }
}
