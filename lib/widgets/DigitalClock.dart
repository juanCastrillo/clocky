import 'dart:async';
import 'package:flutter/material.dart';

///Analog Clock widget with smooth senconds hand
class DigitalClock extends StatefulWidget {
  bool border = false;

  @override
  State<StatefulWidget> createState() {
    return DigitalClockState(DateTime.now());
  }
}

class DigitalClockState extends State<DigitalClock> {
  DateTime time;

  DigitalClockState(time) : this.time = time ?? DateTime.now();

  initState() {
    super.initState();

    // update clock every second
    Duration updateDuration = Duration(seconds: 1);
    Timer.periodic(updateDuration, update);
  }

  update(Timer timer) {
    if (mounted) {
      // update is only called on live clocks. So, it's safe to update time.
      time = DateTime.now();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        NumberTile(number: time.hour),
        NumberTile(number: time.minute),
        NumberTile(number: time.second),
      ],
    );
  }
}

class NumberTile extends StatelessWidget {

  NumberTile({int number, this.fontSize:60}){
    numberString = number.toString();
    if(numberString.length==1) numberString = "0"+numberString;
    //print(numberString);
  }
  String numberString;
  double fontSize;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Container(
          child: Text(
            numberString,
            style: TextStyle(
                fontFamily: 'Abel',
                fontSize: fontSize
            ),
          ),
        ),
      ),
    );
  }
}
