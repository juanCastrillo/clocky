import 'package:clocky/models/Things.dart';
import 'package:flutter/material.dart';

class WeekDayPicker extends StatefulWidget {
  WeekDayPicker({this.daysSelected, this.getDays, this.selectable: true});

  bool selectable;
  List<bool> daysSelected;
  final List<String> days = ["L", "M", "X", "J", "V", "S", "D"];
  final ValueChanged<List<bool>> getDays;

  @override
  State<StatefulWidget> createState() {
    return WeekDayPickerState();
  }
}

class WeekDayPickerState extends State<WeekDayPicker> {
  @override
  Widget build(BuildContext context) {
    return Row(
        children: widget.days.map((day) {
      var position = widget.days.indexOf(day);
      return WeekDayButton(
        on: widget.daysSelected[position],
        dayText: day,
        onPressed: (bool selected) {
          widget.daysSelected[position] = selected;
          widget.getDays(widget.daysSelected);
        },
        selectable: widget.selectable,
      );
    }).toList());
  }
}

class WeekDayButton extends StatefulWidget {
  WeekDayButton({this.on, this.dayText, this.onPressed, this.selectable: true});

  bool on;
  String dayText;
  final ValueChanged<bool> onPressed;
  bool selectable;

  @override
  State<StatefulWidget> createState() => WeekDayButtonState();
}

class WeekDayButtonState extends State<WeekDayButton> with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: FlatButton(
        splashColor: Colors.transparent,  
        highlightColor: Colors.transparent, // makes highlight invisible too
        onPressed: () {
          if (widget.selectable) {
            setState(() {
              widget.on = !widget.on;              
            });
            widget.onPressed(widget.on);            
          }
        },
        shape: CircleBorder(),
        // TODO - Animate color change
        color: widget.on ? Theme.of(context).accentColor : Theme.of(context).backgroundColor,
        child: Text(
          widget.dayText, 
          style: Theme.of(context).textTheme.bodyText2
        ),
      ),
    );
  }
}
