import 'package:clocky/models/Alarm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'WeekDayPickerWidget.dart';

//TODO adapt to show when its shared alarm or just local
//TODO - Add elemet to allow for editing the alarm.

///A card to show an Alarm object
class AlarmCard extends StatefulWidget {
  AlarmCard({this.alarm, this.editAlarm, this.removeAlarm, this.toggleAlarm});

  Alarm alarm;
  Function editAlarm;
  Function removeAlarm;
  Function toggleAlarm;


  @override
  State<StatefulWidget> createState() {
    return AlarmCardState();
  }
}

class AlarmCardState extends State<AlarmCard> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom:8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        widget.alarm.time.toString(),
                        style: TextStyle(fontSize: 32.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        widget.alarm.description,
                        style: TextStyle(
                          fontSize: 17.0,
                        ),
                        textAlign: TextAlign.left,

                      ),
                    ),
                    Spacer(),
                    IconButton(
                      icon: Icon(Icons.edit_attributes),
                      color: Colors.amber, 
                      onPressed: () { widget.editAlarm(); }),
                    Switch(
                      value: widget.alarm.active,
                      onChanged: (changed){
                        widget.toggleAlarm();
                        // setState(() {
                        //   widget.alarm.active = !widget.alarm.active;
                        // });
                      },
                      activeColor: Theme.of(context).accentColor,)
                  ],
                ),
              ),
              WeekDayPicker(
                daysSelected: widget.alarm.days,
                selectable: false,
              ),
            ],
          ),
        ),
      ),
    );
  }
}


