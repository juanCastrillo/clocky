import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:clocky/models/Alarm.dart';
import 'package:clocky/models/Time.dart';
import 'TimePicker.dart';
import 'WeekDayPickerWidget.dart';

///A card design to create Alarm objects
class AlarmCreationCard extends StatefulWidget {
  AlarmCreationCard({this.alarm, this.saveAlarm, this.removeAlarm});

  Alarm alarm;
  final ValueChanged<Alarm> saveAlarm;
  final ValueChanged<Alarm> removeAlarm;

  @override
  State<StatefulWidget> createState() {
    return AlarmCreationCardState();
  }
}

class AlarmCreationCardState extends State<AlarmCreationCard> {

  TextEditingController ctlr;

  initState() {
    super.initState();
    ctlr = TextEditingController()..text = widget.alarm.description;  
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2,
        color: Theme.of(context).splashColor.withOpacity(0.9),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextField(
                controller: ctlr,            
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 8),
                  labelText: "Description",
                  prefixIcon: Icon(Icons.calendar_today),
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: ctlr.text.isNotEmpty? Colors.amber : Colors.black54
                    ),
                  )
                ),
                textAlign: TextAlign.start,
              ),
              Padding(padding: EdgeInsets.all(12.0),),
              TimePicker(
                time: widget.alarm.time,
                getTime: (Time time){widget.alarm.time=time;},
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      widget.alarm.description,
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ],
              ),
              WeekDayPicker(
                daysSelected: widget.alarm.days,
                selectable: true,
                getDays: (List<bool> days){widget.alarm.days = days;},
              ),
              Row(
                children: [
                  FlatButton(
                    child: Text("Add"),
                    onPressed: () {
                      widget.alarm.description = ctlr.text;
                      widget.alarm.editing = true;
                      //print(widget.alarm.toString());
                      widget.saveAlarm(widget.alarm);
                    },
                  ),
                  FlatButton(
                    child: Icon(Icons.close),
                    onPressed: () {
                      widget.alarm.description = ctlr.text;
                      widget.alarm.editing = true;
                      //print(widget.alarm.toString());
                      widget.removeAlarm(widget.alarm);
                    },
                  ),
                ],
              ),              
            ],
          ),
        ),
      ),
    );
  }
}
