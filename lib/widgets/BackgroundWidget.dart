import 'package:flutter/material.dart';

class BackgroundGradient extends StatelessWidget {

  BackgroundGradient({this.orientation, this.inverted:false, this.startColor, this.endColor, this.middleColor}){
    switch(orientation){

      case Orientation.portrait:{
        if(!inverted){
          start = Alignment.topCenter;
          end = Alignment.bottomCenter;
        } else {
          start = Alignment.bottomCenter;
          end = Alignment.topCenter;
        }
      }
        break;

      case Orientation.landscape: {
        if(!inverted){
          start = Alignment.centerLeft;
          end = Alignment.centerRight;
        } else {
          start = Alignment.centerRight;
          end = Alignment.centerLeft;
        }
      }
        break;
    }

    if(middleColor == null)
    colors = [startColor, endColor];
    else colors = [startColor, middleColor, endColor];
  }

  Alignment start;
  Alignment end;
  bool inverted;
  Orientation orientation;

  Color startColor;
  Color middleColor;
  Color endColor;

  List<Color> colors;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: colors,
          end: end,
          begin: start,
        ),
      ),
    );
  }

}