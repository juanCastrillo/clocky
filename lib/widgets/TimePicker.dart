import 'package:clocky/models/Things.dart';
import 'package:clocky/models/Time.dart';
import 'package:flutter/material.dart';

///A simple picker for hours and minutes
class TimePicker extends StatefulWidget {

  TimePicker({this.time, this.getTime});
  Time time;

  final ValueChanged<Time> getTime;

  @override
  State<StatefulWidget> createState() {
    return TimePickerState();
  }
}

///2 Roller lists with hours and 5 minute intervals
class TimePickerState extends State<TimePicker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(2.0),
      // decoration: BoxDecoration(
      //   border:Border.all(width: 2, color: Theme.of(context).backgroundColor),
      //   borderRadius: BorderRadius.circular(8)
      // ),
      child: SizedBox(
        height: 100,
        width: 150,

        child: Stack(

          children:<Widget>[
            //Center(child: Text(":")),
            Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor.withOpacity(0.5),
                ),
                height: 40,
              ),
            ),
            Row(
              children: <Widget>[
                NumberPicker(
                  start: widget.time.hour,
                  numList: hours,
                  getNumber: (int hour){widget.time.hour = hour; widget.getTime(widget.time);},
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  // child: Container(
                  //   color: Colors.black54,
                  //   width: 1.0,
                  // ),
                  child: Text(":")
                ),
                NumberPicker(
                  start: widget.time.minute,
                  numList: minutes,
                  getNumber: (int minute){widget.time.minute = minute; widget.getTime(widget.time);},
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class NumberPicker extends StatefulWidget {

  NumberPicker({this.start = 0, this.numList, this.getNumber});

  int start;
  List<num> numList;
  final ValueChanged<int> getNumber;

  @override
  State<StatefulWidget> createState() {
    return NumberPickerState();
  }

}

class NumberPickerState extends State<NumberPicker> {

  FixedExtentScrollController ctlr = FixedExtentScrollController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) { ctlr.animateToItem(
        widget.start, 
        duration: Duration(milliseconds: 500), 
        curve: Curves.decelerate,
      ); });
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      

    return Flexible(
      child: ListWheelScrollView.useDelegate(
        controller: ctlr,
        onSelectedItemChanged: (int i){widget.getNumber(widget.numList[i]);},
        perspective: 0.01,
        useMagnifier: true,
        itemExtent: 38.0,
        magnification: 1.2,
        physics: FixedExtentScrollPhysics(),
        childDelegate: ListWheelChildBuilderDelegate(
          childCount: widget.numList.length,
          builder: (BuildContext context, int i){
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal:8.0, vertical: 1.0),
              child: Number(
                number: widget.numList[i],
              ),
            );
          },
        ),
      ),
    );
  }
}

class Number extends StatelessWidget {

  Number({this.number});
  int number;
  String text;

  @override
  Widget build(BuildContext context) {

    text = (number.toString().length < 2) ?
       "0" + number.toString() : number.toString();

    return Container(
      height: 60,
      width: 150,
      color: Theme.of(context).splashColor,
      child: Center(
        child: Text(
          text,
          style: TextStyle(fontFamily: 'Abel', fontSize: 28),
        ),
      ),
    );
  }

}