import 'dart:async';
import 'package:flutter/material.dart';

import 'bloc/AlarmsBLoC.dart';
import 'models/Alarm.dart';
import 'widgets/AlarmCardWidget.dart';
import 'widgets/AlarmCreationWidget.dart';

AlarmsBLoC bloc = AlarmsBLoC();

///Collection of widgets to manage and view alarms
///and the information contained in them
class AlarmsPage extends StatefulWidget {
  AlarmsPage({Key key}) : super(key: key);

  @override
  _AlarmsPageState createState() => _AlarmsPageState();
}

class _AlarmsPageState extends State<AlarmsPage> {
  Stream<List<Alarm>> alarmStream;

  @override
  void initState() {
    alarmStream = bloc.stream_alarms;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Center(
              child: StreamBuilder<List<Alarm>>(
                  stream: alarmStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      bloc.update();
                      return CircularProgressIndicator();
                    } else {
                      print("Alarmas?: " + snapshot.data.toString()+"\n");
                      return ListView.builder(
                          padding: EdgeInsets.all(0.0),
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int i) {                        
                            Alarm alarm = snapshot.data[i];
                            if (!alarm.editing) {
                              return Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                  ),
                                  Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context).accentColor),
                                  ),
                                  Expanded(
                                      child: AlarmCard(
                                    alarm: alarm,
                                    editAlarm: () {
                                      alarm.editing = true;
                                      bloc.updateAlarm(alarm);
                                    },
                                    removeAlarm: () {
                                      bloc.removeAlarm(alarm);
                                    },
                                    toggleAlarm: () {
                                      alarm.active = !alarm.active;
                                      //TODO - use setAlarm method
                                      bloc.updateAlarm(alarm);
                                    },
                                  )),
                                ],
                              );
                            } else
                              return AlarmCreationCard(
                                  alarm: alarm,
                                  saveAlarm: (Alarm alarm) {
                                    alarm.editing = false;
                                    bloc.updateAlarm(alarm);
                                  },
                                  removeAlarm: (Alarm alarm) {
                                    bloc.removeAlarm(alarm);
                                  });
                          });
                    }
                  }),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => bloc.createTempAlarm(),
        tooltip: 'Add',
        child: Icon(Icons.add),
        foregroundColor: Theme.of(context).textTheme.bodyText2.color,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
//  @override
//  void dispose() {
//    widget.bloc.dispose();
//    super.dispose();
//  }
}
