import 'package:clocky/models/Exceptions.dart';
import 'package:clocky/models/Time.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){
  test("Time constructor hour overflow",(){
    var exceptionThrown;
    try {
      Time(hour: 25, minute: 40);
      exceptionThrown = false;
    }catch(HourOutOfRangeException){exceptionThrown = true;}
      expect(exceptionThrown, true);
  });

  test("Time constructor minute overflow",(){
    var exceptionThrown;
    try {
      Time(hour: 20, minute: 63);
      exceptionThrown = false;
    }on MinuteOutOfRangeException {exceptionThrown = true;}
    expect(exceptionThrown, true);
  });

  test("Time from correct string", (){
    var correctTime = Time(hour: 21, minute: 22);
    var time = Time.fromString("21:22");
    expect(time.toString(), correctTime.toString());
  });

  test("Time from wrong string", (){
    var exceptionThrown;
    try{
      Time.fromString("fodsanekns");
      exceptionThrown = false;
    }on WrongTimeStringFormatException {exceptionThrown = true;}

    expect(exceptionThrown, true);
  });

  test("Time to string",(){
    var correctString = "12:31";
    var string = Time(hour:12, minute: 31).toString();
    expect(correctString, string);
  });
}