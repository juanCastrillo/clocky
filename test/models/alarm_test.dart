import 'package:clocky/models/Alarm.dart';
import 'package:clocky/models/Time.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){

  test("Alarm constructor wrong properties", (){
    var description = null;
    var time = Time(hour: 24, minute: 13);
    var days = [false, false, true, true, false, false, true];

    var alarm = Alarm(description: description, time: time, days: days);

    expect(dynamic, alarm);
  });

  test("Json alarm conversion", (){

    var description = "Testing";
    var time = "12:19";
    var formatTime = Time.fromString(time);
    var days = [false, false, true, true, false, false, true];
    var alarmJson = {
      "description": description,
      "time": time,
      "days": days
    };

    var alarm = Alarm(
      description: description,
      time: formatTime,
      days: days,
    );

    expect(Alarm.fromJson(alarmJson).toString(), alarm.toString());

  });

  test("Json alarm conversion with more days test", (){
    var description = "Testing";
    var time = "12:19";
    var formatTime = Time.fromString(time);
    var days = [false, false, true, true, false, false, true, false, true];
    var alarmJson = {
      "description": description,
      "time": time,
      "days": days
    };

    var alarm = Alarm(
      description: description,
      time: formatTime,
      days: days,
    );

    expect(Alarm.fromJson(alarmJson).toString(), alarm.toString());
  });
}