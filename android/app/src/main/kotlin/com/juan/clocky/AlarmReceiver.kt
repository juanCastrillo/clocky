package com.juan.clocky

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.WindowManager
import androidx.core.app.NotificationCompat


class AlarmReceiver: BroadcastReceiver() {
    @SuppressLint("WrongConstant")
    override fun onReceive(context: Context?, intent: Intent?) {
        val alarmData = intent?.action //?: "AlarmComes"
        if(alarmData != null) {
            Log.d("AlarmReceiver", "Si es la alarma")
            Log.d("AlarmReceiver", alarmData.toString())

            val alarmId = intent!!.getStringExtra("alarmId") ?: "0"
            val description = intent.getStringExtra("alarmDescription") ?: "Description not provided"

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {

                Log.d("AlarmReceiver", "Notification method")
                val id = alarmId.toInt()

                val intentK = Intent(context, MainActivity::class.java)
                intentK.putExtra(EXTRA_ALARM, alarmId)
                val intentX =
                        PendingIntent.getActivity(
                                context!!,
                                0,
                                intentK,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        )

                val builder = NotificationCompat.Builder(context, CHANNEL_ALARM)
                    .setSmallIcon(R.drawable.baseline_alarm_black_24dp)
                    .setContentTitle("$description")
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setFullScreenIntent(intentX, true)

                val mgr = context.getSystemService(NotificationManager::class.java)

                if ( mgr.getNotificationChannel(CHANNEL_ALARM) == null) {
                    mgr.createNotificationChannel(
                            NotificationChannel(
                                    CHANNEL_ALARM,
                                    "Alarm",
                                    NotificationManager.IMPORTANCE_HIGH
                            )
                    )
                }

                mgr.notify(id, builder.build())

                Log.d("AlarmReceiver", "Notification sent")

            } else {

                Log.d("AlarmReceiver", "Activity start method")

                val intentX = Intent(context, MainActivity::class.java)
                intentX.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intentX.putExtra(EXTRA_ALARM, "yes")
                context?.startActivity(intentX)

                Log.d("AlarmReceiver", "Activity started")
            }



//            val launch_intent = Intent("android.intent.action.MAIN")
//            launch_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            launch_intent.putExtra("some_data", "value")
//            context!!.startActivity(launch_intent)

            Log.d("AlarmReceiver", "Fin de alarma")
        }
        else {
            Log.d("AlarmReceiver", "No lo ha llamado una alarma")
        }
    }

}