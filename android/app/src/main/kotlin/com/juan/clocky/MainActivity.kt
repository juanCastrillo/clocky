package com.juan.clocky

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneOffset
import java.util.*


class MainActivity: FlutterActivity() {

    private val CHANNEL = "clocky/alarms"
    private lateinit var alarmManager:AlarmManager
    private var alarmSounded = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        if (intent.hasExtra(EXTRA_ALARM)) {
            alarmSounded = true
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                //TODO - Fix notification not desapearing
                val almgr = getSystemService(NotificationManager::class.java) as NotificationManager
                almgr.cancel(intent.getIntExtra(EXTRA_ALARM, 0))
            }
        }

        registerReceiver(AlarmReceiver(), IntentFilter())
        alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        MethodChannel(flutterView, CHANNEL).setMethodCallHandler{
            call, result ->
            run {
                Log.d("MethodChannel Android","Hi, im here")
                when(call.method){
                    "activateAlarms" -> {
                        //result.success("this comes through")

                        val hourList = call.argument<ArrayList<Int>>("time")
                        val daysList = call.argument<ArrayList<Boolean>>("days")

                        val hour = LocalTime.of(hourList!![0], hourList[1])

                        val alarmIds = activateAlarms(hour, daysList!!, "Thingy")
                        if(alarmIds != null) result.success(alarmIds)
                        else result.error("idk", "idk", 0)
                    }
                    "alarmCall" -> {
                        result.success(alarmSounded)
                    }
                    else -> result.success("no method called xddx")
                }
            }
        }



    }

    fun activationMills(time:LocalDateTime):Long = time.toInstant(ZoneOffset.UTC).toEpochMilli()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun activateAlarms(time:LocalTime, days:List<Boolean>, description: String):List<Int>?{

        val generator = Random()

        val alarmIds = arrayListOf<Int>(0,0,0,0,0,0,0)

        // Time for repeat, all week to go back to same week day next week
        val millsInWeek:Long = 24*3600*1000*7

        // Go through all days of the week
        for(i in 0..6) {

            //set the alarm if neccesary
            if(days[i]) {
                val calendar: Calendar = Calendar.getInstance().apply {
                    timeInMillis = System.currentTimeMillis()
                    set(Calendar.DAY_OF_WEEK, Calendar.MONDAY+i)
                    set(Calendar.HOUR_OF_DAY, time.hour)
                    set(Calendar.MINUTE, time.minute)
                }
                val check = Calendar.getInstance()
                check.timeInMillis = System.currentTimeMillis()
                Log.d("activateAlarms", "${calendar.timeInMillis} > ${System.currentTimeMillis()}")

                // Create alarm Id
                val alarmId = generator.nextInt()

                // Intent to open Activity
                val activityIntent = Intent(
                        this,
                        MainActivity::class.java
                )

                // Data for the intent
                activityIntent.putExtra("alarmId", alarmId.toString())
                activityIntent.putExtra("alarmDescription", description)
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Set the alarm for the apropiated hour
                alarmManager.setAlarmClock(
                        AlarmManager.AlarmClockInfo(
                                calendar.timeInMillis,
                                PendingIntent.getActivity(
                                        this,
                                        0,
                                        activityIntent,
                                        0
                                )
                        ),
                        PendingIntent.getBroadcast(
                                this,
                                0,
                                Intent(this, AlarmReceiver::class.java),
                                0
                        )
                )

                // Set the alarm id once done
                alarmIds[i] = alarmId
            }
        }

        return alarmIds
    }

    fun cancelAlarms(alarmIds:List<Int>){
        alarmIds.forEach{
            alarmManager.cancel(PendingIntent.getActivity(this, it, Intent(this, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT))
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                or WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                or WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

}
